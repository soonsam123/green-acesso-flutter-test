import 'package:flutter/material.dart';
import 'package:flutter_app/colors.dart';
import 'package:flutter_svg/flutter_svg.dart';

class TopRateCard extends StatelessWidget {
  final String name, imgUrl, htmlUrl;
  final int id;

  TopRateCard({this.name, this.imgUrl, this.htmlUrl, this.id});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 15),
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: white,
        boxShadow: [BoxShadow(
          color: black.withOpacity(0.1),
          blurRadius: 5,
          spreadRadius: 5,
          offset: Offset(0, 0)
        )]
      ),
      child: Row(
        children: [

          // User Image
          Container(
            width: 70,
            height: 70,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              image: DecorationImage(
                image: NetworkImage(this.imgUrl)
              )
            ),
          ),

          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [

                  // User name
                  Text(this.name, style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: black)),

                  SizedBox(height: 15),
                  
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [

                      // user html github page
                      Expanded(child: Text(this.htmlUrl, style: TextStyle(fontSize: 16, color: black.withOpacity(0.5)))),
                      
                      // User Id
                      Row(
                        children: [
                          
                          SvgPicture.asset('assets/icons/star.svg', height: 20, color: yellow),

                          SizedBox(width: 5),

                          Text(this.id.toString(), style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: black.withOpacity(0.7))),

                        ],
                      ),

                    ],
                  ),

                ],
              ),
            ),
          ),

        ],
      ),
    );
  }
}