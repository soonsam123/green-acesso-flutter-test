import 'package:flutter/material.dart';
import 'package:flutter_app/colors.dart';
import 'package:flutter_app/views/home/cards/category_card.dart';

class Categories extends StatelessWidget {

  static final Map<int, dynamic> info = {
    0: {
      'icon': 'assets/icons/js.svg',
      'category': 'JavaScript',
      'qtd': '26'
    },
    1: {
      'icon': 'assets/icons/flutter.svg',
      'category': 'Flutter',
      'qtd': '18'
    },
    2: {
      'icon': 'assets/icons/html.svg',
      'category': 'HTML 5',
      'qtd': '32'
    },
    3: {
      'icon': 'assets/icons/python.svg',
      'category': 'Python',
      'qtd': '26'
    },
  };

  @override
  Widget build(BuildContext context) {

    List<Widget> categories = [];

    info.forEach((pos, category) {
      categories.add(CategoryCard(
        icon: category['icon'],
        category: category['category'],
        qtd: category['qtd'],
      ));
    });

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [

        // Texts
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Category', style: TextStyle(fontSize: 22, color: black, fontWeight: FontWeight.w500),),
              Text('See all', style: TextStyle(fontSize: 18, color: black.withOpacity(0.6), fontWeight: FontWeight.w500))
            ],
          ),
        ),

        SizedBox(height: 15),

        // Cards
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: categories,
          ),
        )

      ],
    );
  }
}