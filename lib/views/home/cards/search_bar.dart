import 'package:flutter/material.dart';
import 'package:flutter_app/colors.dart';

class MainSearchBar extends StatelessWidget {

  final double _height = 50;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: _height,
      padding: EdgeInsets.only(left: 15),
      decoration: BoxDecoration(
        color: white,
        borderRadius: BorderRadius.circular(5),
        boxShadow: [BoxShadow(
          color: black.withOpacity(0.1),
          blurRadius: 5,
          spreadRadius: 5,
          offset: Offset(0, 0)
        )]
      ),
      child: Row(
        children: [

          Expanded(
            child: TextField(
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: 'Search...',
                hintStyle: TextStyle(fontSize: 18, color: black.withOpacity(0.4))
              ),
            ),
          ),

          Container(
            width: _height,
            height: _height,
            decoration: BoxDecoration(
              color: green,
              borderRadius: BorderRadius.circular(5)
            ),
            child: Icon(Icons.search, color: white, size: _height-20,),
          )

        ],
      ),
    );
  }
}