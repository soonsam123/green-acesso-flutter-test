import 'package:flutter/material.dart';
import 'package:flutter_app/colors.dart';

class MainTitle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        text: 'Hi, Olivia\n',
        style: TextStyle(fontSize: 25, color: black, fontWeight: FontWeight.w300),
        children: <TextSpan>[
          TextSpan(
            text: 'Welcome Back',
            style: TextStyle(fontSize: 28, fontWeight: FontWeight.w600),
          )
        ]
      ),
    );
  }
}