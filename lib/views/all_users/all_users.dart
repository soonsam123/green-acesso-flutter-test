import 'package:flutter/material.dart';
import 'package:flutter_app/blocs/users_bloc.dart';
import 'package:flutter_app/colors.dart';
import 'package:flutter_app/views/common_widgets/app_bar.dart';
import 'package:flutter_app/views/common_widgets/top_rate_card.dart';
import 'package:provider/provider.dart';

class AllUsersPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    // Bloc
    UsersBloc usersBloc = Provider.of<UsersBloc>(context);

    final double _marginY = 25;
    
    return Scaffold(
      backgroundColor: blue,
      body: SafeArea(
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [

              // AppBar
              MainAppBar(hasBackButton: true,),

              // Body
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.fromLTRB(20, _marginY, 20, 0),
                    decoration: BoxDecoration(
                      color: backgroundColor,
                      borderRadius: BorderRadius.only(topLeft: Radius.circular(30), topRight: Radius.circular(30))
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [

                        // Title
                        Text('All programmers from GitHub', style: TextStyle(fontSize: 25, color: black, fontWeight: FontWeight.w300),),

                        SizedBox(height: _marginY),

                        ListView.builder(
                          itemCount: usersBloc.users.length,
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemBuilder: (BuildContext context, int i) {
                            var user = usersBloc.users[i];
                            return TopRateCard(
                              imgUrl: user['avatar_url'],
                              name: user['login'],
                              id: user['id'],
                              htmlUrl: user['html_url'],
                            );
                          },
                        ),

                      ],
                    ),
                  ),
                ),
              ),

            ]
          ),
        )
      ),
    );
  }
}