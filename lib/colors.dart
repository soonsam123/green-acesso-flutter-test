import 'package:flutter/material.dart';

const Color backgroundColor = Color(0xfff6f6f6);

const Color black = Color(0xff363636);
const Color white = Color(0xfffefefe);
const Color blue = Color(0xff053f5e);
const Color green = Color(0xff107163);
const Color lightGreen = Color(0xff1f7b6e);
const Color yellow = Color(0xffffd700);